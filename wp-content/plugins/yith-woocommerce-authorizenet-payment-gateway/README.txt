=== YITH WooCommerce Authorize.net Payment Gateway ===

Contributors: yithemes
Tags: authorize.net, woocommerce, products, themes, yit, e-commerce, shop, payment gateway, yith, woocommerce authorize.net payment gateway, woocommerce 2.3 ready, credit card, authorize
Requires at least: 4.0.0
Tested up to: 4.5.1
Stable tag: 1.0.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

YITH Authorize.net allows your users to pay using the Authorize.net network services. It requires WooCommerce to work. WooCommerce 2.3.x compatible.

== Description ==

YITH Authorize.net Payment Gateway extends the WooCommerce default payment modes, adding the possibility to pay with credit cards through the services offered by the Authorize.net network. A system that indeed includes an amount of reliability, with the guarantee that your purchase process will not be stopped by lost payments. In addition, research have proved that the more payment methods are available on your website, the more website's reliability is improved: this means that adding credit card will persuade more in choosing your shop to purchase.

= Main features: =

*   Use of Authorize.net for the payment system
*   Neat redirect to Authorize.net's pages for the conclusion of the payment
*   A completely automatic Instant Payment Notification service managed by Authorize.net

== Installation ==

1. Unzip the downloaded zip file.
2. Upload the plugin folder into the `wp-content/plugins/` directory of your WordPress site.
3. Activate `YITH WooCommerce Authorize.net Payment Gateway` from Plugins page

YITH WooCommerce Authorize.net Payment Gateway will add a new submenu called "Authorize.net" under "YIT Plugins" menu. Here you are able to configure all the plugin settings.

== Changelog ==

= 1.0.8 - Released: May, 05 - 2016 =

* Added: Support to WordPress 4.5.1
* Added: Support to WooCommerce 2.5.5
* Added: js code to keep user data through update_checkout process
* Tweak: Removed deprecated WC functions/methods

= 1.0.7 - Released: Jan, 13 - 2016 =

* Added: option to customize "Pay button"
* Added: WC 2.5-RC compatibility
* Added: WP 4.4 compatibility
* Tweak: Performance improved with new plugin core 2.0

= 1.0.6 - Released: Aug, 13 - 2015 =

* Added: Compatibility with WP 4.2.4
* Added: Compatibility with WC 2.4.2
* Tweak: Updated internal plugin-fw

= 1.0.5 - Released: Jul, 03 - 2015 =

* Tweak: formatted order amount with number_format() function
* Tweak: formatted relay url with user_trailingslashit() function
* Fixed: Fingerprint calculation for SIM

= 1.0.4 - Released: Jun, 19 - 2015 =

* Added: WooCommerce 2.3.11
* Fixed: Fingerprint calculation for prices without decimals

= 1.0.3 - Released: May, 04 - 2015 =

* Fixed: "Plugin Documentation" link appearing on all plugins
* Fixed: minor bugs

= 1.0.2 - Released: Apr, 29 - 2015 =

* Added: handling for "Authorize only" transactions
* Fixed: escaped add_query_arg and remove_query_arg

= 1.0.1 - Released: Mar, 09 - 2015 =

* Updated: removed unused libraries
* Fixed: minor fixes

= 1.0.0 - Released: Feb, 20 - 2015 =

* Initial release

== Suggestions ==

If you have suggestions about how to improve YITH Authorize.net Payment Gateway, you can [write us](mailto:plugins@yithemes.com "Your Inspiration Themes") so we can bundle them into YITH Authorize.net Payment Gateway.

== Screenshots ==

1. Settings
2. Authorize.net Checkout panel
3. Authorize.net payment form
4. Order complete after Authorize.net payment

== Translators ==

= Available Languages =
* English (Default)

If you have created your own language pack, or have an update for an existing one, you can send [gettext PO and MO file](http://codex.wordpress.org/Translating_WordPress "Translating WordPress")
[use](http://yithemes.com/contact "Your Inspiration Themes") so we can bundle it into YITH Authorize.net Payment Gateway Languages.

== Documentation ==

Full documentation is available [here](http://yithemes.com/docs-plugins/yith-woocommerce-authorizenet-payment-gateway).

== Upgrade notice ==

= 1.0.8 =

* Added: Support to WordPress 4.5.1
* Added: Support to WooCommerce 2.5.5
* Added: js code to keep user data through update_checkout process
* Tweak: Removed deprecated WC functions/methods