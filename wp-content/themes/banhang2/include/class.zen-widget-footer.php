<?php
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

class ZenWidgetFooter extends WP_Widget {
	protected static $instance;
	protected static $option;

	public function __construct() {
		//Khởi tạo thông số cho Widget
		parent::__construct(
			'zen_widget_footer', // Base ID
			__('Zen Theme Widget Footer', 'nielvmms') // Name			
		);
	}

	public static function getInstance() {
		if (self::$instance === null) {
			self::$instance = new ZenWidgetFooter();
		}
		return self::$instance;
	}

	public static function run() {
		$instance = self::getInstance();
		add_action('widgets_init', function() {
			register_widget('ZenWidgetFooter');
		});
		
		return $instance;
	}

	public function widget($args, $instance) {
		$title = isset($instance[ 'title' ]) ? apply_filters('widget_title', $instance['title']) : '';
		$content = isset($instance[ 'content' ]) ? $instance['content'] : '';
		
		require(QH_THEME_DIR . 'widget-footer-show.php');
	}

	public function form($instance) {
		$title = isset($instance[ 'title' ]) ? apply_filters('widget_title', $instance['title']) : '';
		$content = isset($instance[ 'content' ]) ? $instance['content'] : '';
		require(QH_THEME_DIR . 'widget-footer-form.php');
	}

	public function update($newInstance, $oldInstance) {
		$instance = array();
		$instance['title'] = ( ! empty( $newInstance['title'] ) ) ? strip_tags( $newInstance['title'] ) : '';
		$instance['content'] = isset($newInstance[ 'content' ]) ? $newInstance['content'] : '';
		return $instance;
	}

}