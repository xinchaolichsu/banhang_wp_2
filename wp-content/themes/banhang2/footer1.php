<div class="zfooter container">
	<div class="zfc zfc-one col-sm-4 col-xs-12">
		<?php dynamic_sidebar('footer-sidebar-one'); ?>
	</div>
	<div class="zfc zfc-three col-sm-4 col-xs-12">
		<h4 class="z-title">Kết nối với The Shoe Store</h4>
		<ul>
			<li class="ftSocials mbs"><a class="icon cms-sprite-footer i-socFacebook js-footer-sprite" href="http://www.facebook.com/tiendatstore" target="_blank" title="tiendatstore Facebook"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>	
			<li class="ftSocials mbs"><a class="icon cms-sprite-footer i-socInstagram js-footer-sprite" href="http://instagram.com/tiendatstorevietnam" target="_blank" title="tiendatstore Instagram"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a></li>
			<li class="ftSocials mbs"><a class="icon cms-sprite-footer i-socYoutube js-footer-sprite" href="http://www.youtube.com/tiendatstoreVietnam" target="_blank" title="tiendatstore Youtube"><i class="fa fa-youtube" aria-hidden="true"></i> Youtube</a></li>
			<li class="ftSocials mbs"><a class="icon cms-sprite-footer i-socGooglePlus js-footer-sprite" rel="publisher" href="http://google.com/+tiendatstoreVn" target="_blank" title="tiendatstore Google+"><i class="fa fa-google-plus" aria-hidden="true"></i> Google+</a></li>
		    <li><h4>Hotline: 0912345678</h4></li>
		</ul>
	</div>
	<div class="zfc zfc-two col-sm-4 col-xs-12">
		<div style="overflow: auto">
		<h4 class="z-title">THANH TOÁN</h4>
		<ul class="z-pay ui-listHorizontal">
		    <li class="ftPayment ui-listItem icon2 payMasterCard cms-sprite-footer js-footer-sprite">Master Card</li>
		    <li class="ftPayment ui-listItem icon2 payVISA cms-sprite-footer js-footer-sprite">Visa</li>
		    <li class="ftPayment ui-listItem icon2 payCOD cms-sprite-footer js-footer-sprite" style="">COD</li>
		</ul>
		</div>
		<div>
		<h4 class="z-title">Dịch vụ vận chuyển</h4>
		<div class="ftPayments">
            <!-- if you upload a new icon, please update its filename in js-footer-sprite. Dont rename it -->
			<style>
			/*.js-footer-sprite {
			    background-image: url('https://static-vn.zacdn.com/cms/icon/140604-logo.png');
			}*/

			.ftPayments .js-footer-sprite {
			    background-image: url('/wp-content/themes/nielvmms/asset/images/footer_logo_15072015.png');
			}

			.cms-sprite-footer {
			    background-repeat: no-repeat;
			}
			.b-footer__links .box .box {width:100%; padding-right:0;}

			#ttcvina a {background-position: 0 -195px; height: 30px; width: 107px; text-indent: -10000px; display: block;}
			#viettel a {background-position: -83px -8px; height: 28px; width: 75px; text-indent: -10000px; display: block; margin-top:5px;}
			#certify_01 {float:right; width:49%;}
			#certify_01 a {display:block; width:100%; height:78px;}
			#certify_02 {float:left; width:49%; margin-top:5px;}
			#certify_02 a {display:block; width:100%; height:50px; background:url("https://static-vn.zacdn.com/cms/icon/140305-logo.png") 0 -187px no-repeat;background-size: 208px 337px;}
			</style>
			<div id="ttcvina"><a class="cms-sprite-footer js-footer-sprite" target="_blank" href="http://ttcvina.com">TTC</a></div>
			<div id="viettel"><a class="cms-sprite-footer js-footer-sprite" target="_blank" href="http://viettelpost.com.vn/">Viettel</a></div>
			<div id="vnpost"><img data-load="lazy" data-url="https://static-vn.zacdn.com/cms/logos/vnpost_logo_2.png" src="https://static-vn.zacdn.com/cms/logos/vnpost_logo_2.png"></div>

        </div>
		</div>
	</div>
</div>

<?php wp_footer() ?>
</body>
<!-- END BODY -->
</html>